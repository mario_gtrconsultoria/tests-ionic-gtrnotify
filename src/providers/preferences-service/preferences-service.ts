import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppPreferences } from "@ionic-native/app-preferences";

/*
  Generated class for the PreferencesServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PreferencesServiceProvider {

  static get PREF_INITIALIZED() { return 'preferencesInitialized';}
  static get PREF_DISCOVERABLE() { return 'pref_discoverable';}
  static get PREF_NOTIFY_MESSAGES() { return 'pref_notification_messages';}
  static get PREF_NOTIFY_INVITES() { return 'pref_notification_invites';}

  public _preferences:any;

  constructor(public _pref: AppPreferences) {
    console.log('Hello PreferencesServiceProvider Provider');
  }

  public initializePreferences(){
    console.log('initializePreferences');
    this._pref.fetch(PreferencesServiceProvider.PREF_INITIALIZED).then((result) => {
      if(result == null || result == false){
        console.log('initializePreferences with default values');
        this._pref.store('', PreferencesServiceProvider.PREF_INITIALIZED, true);
        this._pref.store('', PreferencesServiceProvider.PREF_DISCOVERABLE, true);

        //initialize in memory preferences
        this._preferences[PreferencesServiceProvider.PREF_DISCOVERABLE] = true;
      }else{
        console.log('preferences obtained from storage');
        let prefs =
          [
            PreferencesServiceProvider.PREF_DISCOVERABLE
          ];

        let thisRef = this;
        this._getAllPreferences(prefs).then(function(results){
            //initialize in memory preferences
            for(let i = 0; i < prefs.length; i++){
              thisRef._preferences[prefs[i]] = results[i];
            }
          }, function (err) {
            // If any of the preferences fail to read, err is the first error
            console.log(err);
          });
      }
    });
  }

  public getPreference(key){
    return this._preferences[key];
  }

  public setPreference(key, value){
    this._preferences[key] = value;//update pref in memory
    this._pref.store('', key, value);//update pref in db
  }

  public _getAllPreferences(prefs){
    return Promise.all(prefs.map((key) => {
      return this._pref.fetch(key);
    }));
  }

  public _getPreference(key){
    return this._pref.fetch(key);
  }
}
