import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { OneSignal } from '@ionic-native/onesignal';
import { Platform } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/Observable/of';

export class ItemMsg {
  constructor(
    public title: string,
    public note: string,
    public icon: string = 'paper-plane',
    public state = 'inactive') { }

  toggleState() {
    this.state = this.state === 'active' ? 'inactive' : 'active';
  }
}

@Injectable()
export class OneSignalServiceProvider {

  msg: ItemMsg;
  noti: Observable<ItemMsg>;

  constructor(public http: HttpClient, private oneSignal: OneSignal,
    private plt: Platform) {
    console.log('Hello OneSignalServiceProvider Provider');

    if (plt.is('mobile')) {
      this.oneSignal.startInit('e91e99c4-c362-4d36-983c-a4e6c9d38f12', '154075907827');
      this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);

      this.oneSignal.handleNotificationOpened().subscribe((data) => {
        // do something when a notification is opened
      });

      this.oneSignal.handleNotificationReceived().subscribe((data) => {
        // do something when notification is received
        //this.donotification(data.payload.title, data.payload.body);
      });

      this.oneSignal.endInit();

      this.oneSignal.sendTag('usuario', 'GTR');
    }

  }

  public donotification(title: string, body: string) {
    this.msg = <ItemMsg>{
      title: title,
      note: body
    }
    console.log('chamando do');

    let noti = Observable.create(observer => {
      observer.next(this.msg);
      observer.complete();
    });
  }

  public onnotification(): Observable<ItemMsg> {
    return of(this.msg);
    // Observable.create(observer => {
    //   this.msg; 
    //   observer.next(true);
    //   observer.complete();
    // } )
  }

}
