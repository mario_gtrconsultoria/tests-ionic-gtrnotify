import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { MomentModule } from 'ngx-moment';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { BarcodePage } from '../pages/barcode/barcode';
import { LoginPage } from '../pages/login/login';
import { PreferencesPage } from "../pages/preferences/preferences";

import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { OneSignalServiceProvider } from '../providers/one-signal-service/one-signal-service';
import { PreferencesServiceProvider } from '../providers/preferences-service/preferences-service';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { OneSignal } from '@ionic-native/onesignal';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { AppPreferences } from '@ionic-native/app-preferences';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    ListPage,
    BarcodePage,
    PreferencesPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MomentModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    ListPage,
    BarcodePage,
    PreferencesPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    LocalNotifications,
    OneSignal,
    BarcodeScanner,
    HttpClient,
    AppPreferences,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthServiceProvider,
    PreferencesServiceProvider
    //OneSignalServiceProvider
  ]
})
export class AppModule {}
