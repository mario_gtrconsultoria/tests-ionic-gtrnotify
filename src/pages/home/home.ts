import { Component } from '@angular/core';
import { NavController, DateTime } from 'ionic-angular';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { OneSignal } from '@ionic-native/onesignal';
import { Platform } from 'ionic-angular';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Pipe } from '@angular/core';
import { AppPreferences } from "@ionic-native/app-preferences";
import { MomentModule } from 'ngx-moment';
@Pipe({
  name: 'reverse'
})
export class ReversePipe {
  transform(value) {
    return value.slice().reverse();
  }
}
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  animations: [
    trigger('itemState', [
      state('inactive', style({
        backgroundColor: '#fff',
        transform: 'scale(1)'
      })),
      state('active', style({
        backgroundColor: '#488aff',
        transform: 'scale(1.05)'
      })),
      transition('inactive => active', animate('200ms ease-in')),
      transition('active => inactive', animate('200ms ease-out'))
    ])
  ]
})

export class HomePage {

  items: Array<ItemMsg>;

  constructor(public navCtrl: NavController,
    private localNotifications: LocalNotifications,
    private oneSignal: OneSignal,
    public plt: Platform,
    private appPreferences: AppPreferences) {

    this.items = [];

    if (plt.is('cordova')) {
      this.oneSignal.startInit('e91e99c4-c362-4d36-983c-a4e6c9d38f12', '154075907827');
      this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);

      this.oneSignal.handleNotificationOpened().subscribe((data) => {
        // do something when a notification is opened
      });

      this.oneSignal.handleNotificationReceived().subscribe((data) => {
        // do something when notification is received
        this.items.push(new ItemMsg(data.payload.title, data.payload.body));
      });
      this.oneSignal.endInit();
      this.oneSignal.sendTag('usuario', 'GTR');
    }
  }

  goPush(msg: string) {
    // Schedule a single notification
    this.localNotifications.schedule({
      id: 1,
      text: msg
    });
    this.items.push(new ItemMsg(
      msg + ' ' + this.items.length,
      'mensagem de status via goPush: ' + msg + '. a mensagem é de tamanho maior para testar com mensagens grandes'));
  }

  deleteMsg(msg: ItemMsg) {
    this.items.splice(this.items.indexOf(msg), 1);
  }
}
export class ItemMsg {
  constructor(
    public title: string,
    public note: string,
    public icon: string = 'paper-plane',
    public state: string = 'inactive',
    public timestamp: Date = new Date()) { }

  toggleState() {
    this.state = this.state === 'active' ? 'inactive' : 'active';
  }
}