import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PreferencesServiceProvider } from '../../providers/preferences-service/preferences-service';
/**
 * https://github.com/smukov/AvI/tree/master/Ionic2
 */

@IonicPage()
@Component({
  selector: 'page-preferences',
  templateUrl: 'preferences.html',
})
export class PreferencesPage {

  public preferences: any;
  public PREF_DISCOVERABLE: string;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public pref: PreferencesServiceProvider) {
    this.preferences = {};

    this.PREF_DISCOVERABLE = PreferencesServiceProvider.PREF_DISCOVERABLE;
  }

  public ionViewWillEnter() {
    this.preferences[PreferencesServiceProvider.PREF_DISCOVERABLE]
      = this.pref.getPreference(PreferencesServiceProvider.PREF_DISCOVERABLE);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PreferencesPage');
  }

  public changePreference(event, key) {
    this.pref.setPreference(key, event.checked);
    /*if(key === this.PREF_DISCOVERABLE){
      this.pref.setIsDiscoverable(event.checked);
    }*/
  }

}
