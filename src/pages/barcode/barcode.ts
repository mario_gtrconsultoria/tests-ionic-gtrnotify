import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { ToastController } from 'ionic-angular';

@Component({
  selector: 'page-barcode',
  templateUrl: 'barcode.html'
})
export class BarcodePage {
  selectedItem: any;
  icons: string[];
  items: Array<{ title: string, note: string, icon: string }>;
  barcode: string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams, private barcodeScanner: BarcodeScanner, private toastCtrl: ToastController) {
    // If we navigated to this page, we will have an item available as a nav param
    this.selectedItem = navParams.get('item');

    // Let's populate this page with some filler content for funzies
    this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
      'american-football', 'boat', 'bluetooth', 'build'];

    this.items = [];
    /*for (let i = 1; i < 11; i++) {
      this.items.push({
        title: 'Item ' + i,
        note: 'This is item #' + i,
        icon: this.icons[Math.floor(Math.random() * this.icons.length)]
      });
    }*/
  }

  itemTapped(event, item) {
    // That's right, we're pushing to ourselves!
    this.navCtrl.push(BarcodePage, {
      item: item
    });
  }

  goBarcode() {
    this.barcodeScanner.scan({
      showTorchButton: true,
      resultDisplayDuration: 0
    }).then(barcodeData => {
      console.log('Barcode data', barcodeData);

      if (barcodeData.cancelled) {
        let toast = this.toastCtrl.create({
          message: 'Operação cancelada!',
          duration: 3000,
          position: 'top'
        });
        toast.present();
        return;
      }

      this.barcode = barcodeData.text; // JSON.stringify( barcodeData );

      this.items.push({
        title: 'Item ' + this.barcode,
        note: barcodeData.format,
        icon: this.icons[Math.floor(Math.random() * this.icons.length)]
      });

      let toast = this.toastCtrl.create({
        message: 'Item ' + this.barcode + ' adicionado !',
        duration: 3000,
        position: 'top'
      });

      toast.present();

    }).catch(err => {
      console.log('Error', err);
    });
  }

}
